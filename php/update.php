<?php

include_once("database.php");

$timestamp = time();

$game_ret = game_exists($_SESSION['game_name']);

if($game_ret === false){
	echo('false');
	exit(0);
}

$player_ret = player_exists(
	$_SESSION['game_name'], $_SESSION['player_name'],
	$_SESSION['player_password'], true
);

if($player_ret === false){
	echo('false');
	exit(0);
}

$gametype_json = json_decode(file_get_contents(
	"../gameconfig/{$game_ret['gametype_name']}.json"
), true);

$st = $pdo->prepare('insert into event values (
	default, :timestamp, :game_id, :player_id, :attribute_id, :new_value
)');

foreach(array_keys($_POST) as $attr){
	if(substr($attr, 0, 4) != "attr"){
		continue;
	}

	$value = $_POST[$attr];

	$attr_num = intval(substr($attr, 4));

	$attribute_definition = $gametype_json['player_templates'][
		$player_ret['template_id']
	]['attributes'][$attr_num];

	if(array_key_exists('validation', $attribute_definition)){
		switch($attribute_definition['validation']){
			case 'signed int':
				$value = intval(trim($value));
				break;

			case 'unsigned int':
				$value = intval(trim($value));
				if($value < 0){
					echo('false');
					exit(0);
				}
				break;

			case 'list':
				$value = intval(trim($value));
				if(
					$value > count($attribute_definition['values']) ||
					$value < 0
				){
					echo('false');
					exit(0);
				}
				break;

			default:
				echo('false');
				exit(0);
		}
	}

	$ret = $st->execute(array(
		':timestamp' => $timestamp,
		':game_id' => $game_ret['game_id'],
		':player_id' => $player_ret['player_id'],
		':attribute_id' => $attr_num,
		':new_value' => $value
	));

	if(!$ret){
		echo('false');
		exit(0);
	}
}

echo('true');
exit(0);

?>
