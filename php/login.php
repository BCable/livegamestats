<?php

include_once("database.php");


if(test_post(array('game_name', 'game_password', 'observe'))){
	$game_array = game_exists($_POST['game_name'], $_POST['game_password']);

	if($game_array === false){
		fail('Invalid game/password.');
	}

	if(test_post(array('events'))){
		$_SESSION['events'] = 1;
	} else {
		unset($_SESSION['events']);
	}

	$_SESSION['observe'] = 1;
	$_SESSION['game_name'] = $game_array['name'];
	$_SESSION['game_password'] = pass_hash(
		$_POST['game_password'], $game_array['salt']
	);

	header("Location: game/{$_POST['game_name']}");
	exit(0);
}

if(test_post(array('game_name', 'player_name', 'player_password'))){
	$game_array = game_exists($_POST['game_name']);

	if($game_array === false){
		fail('Invalid game/password.');
	}

	if(test_post(array('events'))){
		$_SESSION['events'] = 1;
	} else {
		unset($_SESSION['events']);
	}

	$player_array = player_exists(
		$_POST['game_name'], $_POST['player_name'],
		$_POST['player_password']
	);

	if($player_array === false){
		fail('Invalid player/password.');
	}

	unset($_SESSION['observe']);
	$_SESSION['game_name'] = $game_array['name'];
	$_SESSION['player_name'] = $player_array['name'];
	$_SESSION['player_password'] = pass_hash(
		$_POST['player_password'], $player_array['salt']
	);

	header("Location: game/{$_POST['game_name']}");
	exit(0);
}

fail('You must fill out all fields.');

?>
