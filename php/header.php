<?php

$always_display_events = null;
$display_events = null;
$display_gameboard = null;

if(isset($dbconn) && $dbconn !== false){
	include_once("database.php");
}

$stylesheets[] = "global";

?>

<html>
<head>

<?php if(!stripos($_SERVER['HTTP_USER_AGENT'], 'iPad')){ ?>
<meta name="viewport" content="width=device-width, initial-scale=1.0, maximum-scale=1.0, user-scalable=no" />
<meta name="HandheldFriendly" content="true" />
<?php } ?>

<?php foreach($stylesheets as $stylesheet){ ?>
<link rel="stylesheet" type="text/css" href="css/<?php
	echo $stylesheet; ?>.css" />
<?php } ?>

<?php foreach($scripts as $script){ ?>
<script language="javascript" src="js/<?php echo $script; ?>.js"></script>
<?php } ?>

</head>

<body>
