<?php

$dbconn = true;
$stylesheets = array('main');
$scripts = array('helpers', 'main');
include_once('header.php');
include_once('validation.php');

$games = array();
$st = $pdo->prepare('select name, gametype_name from game');
$ret = $st->execute();
if($ret){
	$games = $st->fetchAll(PDO::FETCH_ASSOC);
}
$st = $pdo->prepare('select name, gametype_name from game');

$gametypes = array();
$dir = dir('../gameconfig');
while(false !== ($entry = $dir->read())){
	if($entry == '.' || $entry == '..'){
		continue;
	}
	$gametypes[] = substr($entry, 0, -5);
}

?>

<div id="msg_box"<?php if(!empty($_SESSION['message'])){
	echo " style=\"display: block\">{$_SESSION['message']}";
	unset($_SESSION['message']);
} else {
	echo '>';
} ?></div>

<div id="error_box"<?php if(!empty($_SESSION['error'])){
	echo " style=\"display: block\">Error: {$_SESSION['error']}";
	unset($_SESSION['error']);
} else {
	echo '>';
} ?></div>

<div id="main_box">

<a href="#" id="login" onclick="click_display('login');">Login&nbsp;to&nbsp;Game</a>
<div id="login_inputs">
<form method="post" action="login">
<table cellspacing="0" cellpadding="0">
<tr>
	<th>Game&nbsp;Name:</th>
	<td>
		<select name="game_name">
			<option value="">--</option>
<?php foreach($games as $game){
	echo "<option>{$game['name']}</option>";
} ?>
		</select>
	</td>
</tr>
<tr>
	<th>Player&nbsp;Name:</th>
	<td><input type="text" name="player_name" /></td>
</tr>
<tr>
	<th>Password:</th>
	<td><input type="password" name="player_password" autocomplete="off" /></td>
</tr>
<tr>
	<th>Event&nbsp;Log:</th>
	<td><input type="checkbox" name="events" value="1" /></td>
</tr>
<tr>
	<td colspan="2">
		<input type="submit" value="Login" class="submit" />
	</td>
</tr>
</table>
</form>
</div>

<a href="#" id="observe_game" onclick="click_display('observe_game');">Observe&nbsp;Game</a>
<div id="observe_game_inputs">
<form method="post" action="login">
<input type="hidden" name="observe" value="1" />
<table cellspacing="0" cellpadding="0">
<tr>
	<th>Game&nbsp;Name:</th>
	<td>
		<select name="game_name">
			<option value="">--</option>
<?php foreach($games as $game){
	echo "<option>{$game['name']}</option>";
} ?>
		</select>
	</td>
</tr>
<tr>
	<th>Game&nbsp;Password:</th>
	<td><input type="password" name="game_password" /></td>
</tr>
<tr>
	<th>Event&nbsp;Log:</th>
	<td><input type="checkbox" name="events" value="1" /></td>
</tr>
<tr>
	<td colspan="2">
		<input type="submit" value="Observe Game" class="submit" autocomplete="off" />
	</td>
</tr>
</table>
</form>
</div>

<a href="#" id="new_player" onclick="click_display('new_player');">Create&nbsp;New&nbsp;Player</a>
<div id="new_player_inputs">
<form method="post" action="newplayer">
<table cellspacing="0" cellpadding="0">
<tr>
	<th>Player&nbsp;Name:</th>
	<td><input type="text" name="player_name" autocomplete="off" /></td>
</tr>
<tr>
	<th>Game&nbsp;Name:</th>
	<td>
		<select
			id="player_game_name"
			name="game_name"
			autocomplete="off"
			onchange="update_game();"
		>
			<option value="">--</option>
<?php foreach($games as $game){
	echo "<option>{$game['name']}</option>";
} ?>
		</select>
	</td>
</tr>
<tr id="color_id_row">
	<th>Player&nbsp;Color:</th>
	<td>
		<select name="color_id" id="color_id" autocomplete="off"></select>
	</td>
</tr>
<tr id="template_id_row">
	<th>Player&nbsp;Template:</th>
	<td>
		<select name="template_id" id="template_id" autocomplete="off"></select>
	</td>
</tr>
<tr>
	<th>Game&nbsp;Password:</th>
	<td>
		<input type="password" name="game_password" autocomplete="off" />
	</td>
</tr>
<tr>
	<th>Player&nbsp;Password:</th>
	<td>
		<input type="password" name="player_password" autocomplete="off" />
	</td>
</tr>
<tr>
	<th>Confirm&nbsp;Password:</th>
	<td>
		<input type="password" name="confirm_password" autocomplete="off" />
	</td>
</tr>
<tr>
	<td colspan="2">
		<input type="submit" value="Create New Player" class="submit" />
	</td>
</tr>
</table>
</form>
</div>

<a href="#" id="new_game" onclick="click_display('new_game');">New&nbsp;Game</a>
<div id="new_game_inputs">
<form method="post" action="newgame">
<table cellspacing="0" cellpadding="0">
<tr>
	<th>Game&nbsp;Name:</th>
	<td><input type="text" name="game_name" /></td>
</tr>
<tr>
	<th>Game&nbsp;Type:</th>
	<td>
		<select name="gametype_name">
			<option value="">--</option>
<?php foreach($gametypes as $gametype){
	echo "<option>{$gametype}</option>";
} ?>
		</select>
	</td>
</tr>
<tr>
	<th>Game&nbsp;Password:</th>
	<td>
		<input type="password" name="password" autocomplete="off" />
	</td>
</tr>
<tr>
	<th>Confirm&nbsp;Password:</th>
	<td>
		<input type="password" name="confirm_password" autocomplete="off" />
	</td>
</tr>
<tr>
	<td colspan="2">
		<input type="submit" value="Create Game" class="submit" />
	</td>
</tr>
</table>
</form>
</div>

</div>

<script language="javascript">
<!--

window.games = {
<?php foreach($games as $game){
	echo "'{$game['name']}': '{$game['gametype_name']}',";
} ?>
};

window.gametypes = {
<?php foreach($gametypes as $gametype){
	$gametype_json = file_get_contents("../gameconfig/{$gametype}.json");
	echo "'{$gametype}': {$gametype_json},";
} ?>
};

reset_select_values();

//-->
</script>

<?php include_once("footer.php"); ?>
