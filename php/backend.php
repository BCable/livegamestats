<?php

$dbconn = true;
include_once("database.php");
include_once("validation.php");

if(isset($_GET['timestamp'])){
	$timestamp = intval($_GET['timestamp']);
} else {
	$timestamp = 0;
}

$game_array = game_exists($_SESSION['game_name']);

if($game_array === false){
	echo '{}';
}

$st = $pdo->prepare("
	select timestamp, player_id, attribute_id, new_value from event
	where game_id = :game_id and timestamp > :timestamp
	order by timestamp asc
");
$event_ret = $st->execute(array(
	":game_id" => $game_array['game_id'],
	":timestamp" => $timestamp
));

if($event_ret){
	$event_data = $st->fetchAll(PDO::FETCH_ASSOC);
}

$st = $pdo->prepare("
	select name, timestamp, template_id, color_id from player
	where game_id = :game_id and timestamp > :timestamp
	order by timestamp asc
");
$player_ret = $st->execute(array(
	":game_id" => $game_array['game_id'],
	":timestamp" => $timestamp
));

if($player_ret){
	$player_data = $st->fetchAll(PDO::FETCH_ASSOC);
}

$json_array = array(
	"event" => $event_data,
	"player" => $player_data
);

print_r(json_encode($json_array));

?>
