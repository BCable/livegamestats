<?php

session_start();

include_once("database.php");

function fail($errmsg=''){
	if(!empty($errmsg)){
		$_SESSION['error'] = "{$errmsg}";
	}
	header('Location: main');
	exit(0);
}

function success($msg=''){
	if(!empty($msg)){
		$_SESSION['message'] = "{$msg}";
	}
	header('Location: main');
	exit(0);
}

function require_post($post_names){
	if(!test_post($post_names)){
		fail('You must fill out all fields.');
	}
}

function test_post($post_names){
	foreach($post_names as $post_name){
		if(!array_key_exists($post_name, $_POST) || (
				empty($_POST[$post_name]) && $_POST[$post_name] != '0'
		)){
			return false;
		}
	}
	return true;
}

function gametype_exists($gametype_name){
	return file_exists("../gameconfig/{$gametype_name}.json");
}

function game_exists($game_name, $password=null, $hashed=false){
	global $pdo;

	$st = $pdo->prepare('
		select name, game_id, gametype_name, salt from game where name = :name
	');
	$ret = $st->execute(array(':name' => $game_name));

	if(!$ret){
		fail('Database error.');
	}

	$data = $st->fetchAll(PDO::FETCH_ASSOC);

	if(count($data) == 0){
		return false;
	}

	if($password != null){
		$salt = $data[0]['salt'];

		if($hashed === false){
			$pass_hash = pass_hash($password, $salt);
		} else {
			$pass_hash = $password;
		}

		$st = $pdo->prepare('
			select name, game_id, gametype_name, salt from game
			where name = :name and password = :password
		');
		$ret = $st->execute(array(
			':name' => $game_name,
			':password' => $pass_hash
		));

		$data_wpass = $st->fetchAll(PDO::FETCH_ASSOC);

		if(count($data_wpass) > 0){
			return $data_wpass[0];
		} else {
			return false;
		}
	}

	return $data[0];
}

function player_exists($game_name, $player_name, $password=null, $hashed=false){
	global $pdo;

	$st = $pdo->prepare('
		select p.name, p.salt, p.template_id, (
			select count(*) from player as p_int
			left join game as g_int on g_int.game_id = p_int.game_id
			where
				p_int.timestamp < p.timestamp and
				g_int.game_id = g.game_id
			order by timestamp asc
		) as player_id from game as g
		left join player as p on g.game_id = p.game_id
		where g.name = :game_name and p.name = :player_name
	');
	$ret = $st->execute(array(
		':game_name' => $game_name,
		':player_name' => $player_name
	));

	if(!$ret){
		fail('Database error.');
	}

	$data = $st->fetchAll(PDO::FETCH_ASSOC);

	if(count($data) == 0){
		return false;
	}

	if($password != null){
		$salt = $data[0]['salt'];

		if($hashed === false){
			$pass_hash = pass_hash($password, $salt);
		} else {
			$pass_hash = $password;
		}

		$st = $pdo->prepare('
			select p.name, p.salt, p.template_id, (
				select count(*) from player as p_int
				left join game as g_int on g_int.game_id = p_int.game_id
				where
					p_int.timestamp < p.timestamp and
					g_int.game_id = g.game_id
				order by timestamp asc
			) as player_id from game as g
			left join player as p on g.game_id = p.game_id
			where
				g.name = :game_name and
				p.name = :player_name and
				p.password = :password
		');
		$ret = $st->execute(array(
			':game_name' => $game_name,
			':player_name' => $player_name,
			':password' => $pass_hash
		));

		$data_wpass = $st->fetchAll(PDO::FETCH_ASSOC);

		if(count($data_wpass) > 0){
			return $data_wpass[0];
		} else {
			return false;
		}
	}

	return $data[0];
}


function template_exists($gametype_json, $game_name, $template_id){
	global $pdo;
	$template_id = intval($template_id);

	if(
		$template_id < 0 ||
		$template_id > count($gametype_json["player_templates"])
	){
		fail('Invalid player template.');
	}

	if(!array_key_exists("restrict_templates", $gametype_json)){
		return $template_id;
	}

	$st = $pdo->prepare('
		select p.name from player as p
		left join game as g on p.game_id = g.game_id
		where template_id = :template_id and g.name = :game_name
	');
	$ret = $st->execute(array(
		':template_id' => $template_id,
		':game_name' => $game_name
	));

	if(!$ret){
		fail('Database error.');
	}

	$data = $st->fetchAll(PDO::FETCH_ASSOC);
	if(count($data) == 0){
		return $template_id;
	}

	return false;
}

function color_exists($gametype_json, $game_name, $color_id){
	global $pdo;

	if(!array_key_exists("colors", $gametype_json)){
		return true;
	}

	$color_id = intval($color_id);

	if(count($gametype_json["colors"]) <= $color_id || $color_id < 0){
		return false;
	}

	if(!array_key_exists("restrict_colors", $gametype_json)){
		return $color_id;
	}

	$st = $pdo->prepare('
		select p.name from player as p
		left join game as g on p.game_id = g.game_id
		where color_id = :color_id and g.name = :game_name
	');
	$ret = $st->execute(array(
		':color_id' => $color_id,
		':game_name' => $game_name
	));

	if(!$ret){
		fail('Database error.');
	}

	$data = $st->fetchAll(PDO::FETCH_ASSOC);
	if(count($data) == 0){
		return $color_id;
	}

	return false;
}

function salt_gen($salt_bytes=4){
	return bin2hex(openssl_random_pseudo_bytes($salt_bytes));
}

function pass_hash($password, $salt, $numtimes=1){
	$hash_value = null;
	for($i=0; $i<$numtimes; $i++){
		if($hash_value == null){
			$hash_value = hash('sha256', "{$salt}{$password}{$salt}");
		} else {
			$hash_value = hash('sha256', "{$salt}{$hash_value}{$salt}");
		}
	}
	return $hash_value;
}

?>
