<?php

include_once("database.php");

require_post(array(
	'game_name', 'gametype_name', 'password', 'confirm_password'
));

if($_POST['password'] != $_POST['confirm_password']){
	fail('Passwords do not match.');
}

if(gametype_exists($_POST['gametype_name']) === false){
	fail('Invalid game type.');
}

if(game_exists($_POST['game_name']) !== false){
	fail('Game name already exists.');
}

$st = $pdo->prepare('insert into game values(
	default, :name, :password, :salt, :gametype_name
)');

$salt = salt_gen();
$pass_hash = pass_hash($_POST['password'], $salt);

$st->execute(array(
	':name' => $_POST['game_name'],
	':password' => $pass_hash,
	':salt' => $salt,
	':gametype_name' => $_POST['gametype_name']
));

success("Created Game: {$_POST['game_name']}");

?>
