<?php

include_once("database.php");

require_post(array(
	'player_name', 'game_name', 'game_password',
	'player_password', 'confirm_password'
));

if($_POST['player_password'] != $_POST['confirm_password']){
	fail('Passwords do not match.');
}

$ret_array = game_exists($_POST['game_name'], $_POST['game_password']);

if($ret_array === false){
	fail('Invalid game/password.');
}

$game_id = $ret_array["game_id"];
$gametype_name = $ret_array["gametype_name"];

if(player_exists($_POST['game_name'], $_POST['player_name']) !== false){
	fail('Player name already exists.');
}

$gametype_json = json_decode(
	file_get_contents("../gameconfig/{$gametype_name}.json"), true
);

if(test_post(array('template_id'))){
	$template_id = template_exists(
		$gametype_json, $_POST['game_name'],  $_POST['template_id']
	);
	if($template_id === false){
		fail('Invalid player template.');
	}

} else {
	$template_id = 0;
}

$color_id = 0;
if(test_post(array('color_id'))){
	$color_id = $_POST['color_id'];
}
$color_id = color_exists(
	$gametype_json, $_POST['game_name'], $color_id
);

if($color_id === false){
	fail('Invalid color.');
}

$salt = salt_gen();
$pass_hash = pass_hash($_POST['player_password'], $salt);

$st = $pdo->prepare('insert into player values (
	default, :name, :password, :salt,
	:game_id, :template_id, :timestamp, :color_id
)');
$ret = $st->execute(array(
	":name" => $_POST['player_name'],
	":password" => $pass_hash,
	":salt" => $salt,
	":game_id" => $game_id,
	":timestamp" => time(),
	":template_id" => $template_id,
	":color_id" => $color_id
));

if(!$ret){
	fail("Database error.");

} else {
	unset($_SESSION['events']);
	unset($_SESSION['observe']);
	$_SESSION['game_name'] = $ret_array['name'];
	$_SESSION['player_name'] = $_POST['player_name'];
	$_SESSION['player_password'] = $pass_hash;

	header("Location: game/{$_POST['game_name']}");
	exit(0);
}

?>
