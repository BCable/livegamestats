<?php

$dbconn = true;
$stylesheets = array('gameboard');
$scripts = array('helpers', 'game');
include_once("header.php");
include_once("validation.php");

$always_display_events = false;
$display_events = false;
$display_gameboard = true;

if(array_key_exists('events', $_SESSION)){
	$always_display_events = true;
	$display_events = true;

} elseif(array_key_exists('events', $_GET)){
	$display_events = true;
	$display_gameboard = false;
}

if(array_key_exists('observe', $_SESSION)){
	$game_array = game_exists(
		$_SESSION['game_name'], $_SESSION['game_password'], true
	);
	$player_id = 'null';

} else {
	if(test_post(array('events'))){
		$display_events = true;
	}

	$player_array = player_exists(
		$_SESSION['game_name'], $_SESSION['player_name'],
		$_SESSION['player_password'], true
	);

	if($player_array === false){
		fail('Invalid player credentials.');
	}
	$player_id = $player_array['player_id'];
	$game_array = game_exists($_GET['name']);
}

if($game_array === false){
	fail('Invalid game credentials.');
}

?>

<div id="gametitlerow">
	<span id="gametitle"></span><span
	 id="gamecolonspace">:&nbsp;</span><span id="gamecode"></span>
</div>

<div id="gameboard"></div>
<div id="events"></div>

<script language="javascript" defer="defer">
<!--

window.display_events = <?php echo ($display_events?'true':'false'); ?>;
window.display_gameboard = <?php echo ($display_gameboard?'true':'false'); ?>;

load_game(
	<?php echo $game_array['game_id']; ?>,
	"<?php echo $game_array['gametype_name']; ?>",
	"<?php echo $game_array['name']; ?>",
	<?php echo $player_id; ?>
);
//-->
</script>

<?php

include_once("footer.php");

?>
