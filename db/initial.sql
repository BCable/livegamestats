drop database if exists livegamestats;
create database livegamestats;

use livegamestats;

drop table if exists event;
create table event (
	event_id int(10) unsigned primary key not null auto_increment,
	timestamp int(11) unsigned,
	game_id int(4) unsigned,
	player_id int(5) unsigned,
	attribute_id int(4) unsigned,
	new_value varchar(100)
);

drop table if exists game;
create table game (
	game_id int(4) unsigned primary key not null auto_increment,
	name varchar(20),
	password varchar(64),
	salt char(8),
	gametype_name varchar(20)
);

drop table if exists player;
create table player (
	player_id int(5) unsigned primary key not null auto_increment,
	name varchar(100),
	password varchar(64),
	salt char(8),
	game_id int(4) unsigned,
	template_id int(4) unsigned,
	timestamp int(11) unsigned,
	color_id int(2) unsigned
);
