function hide_message_boxes(){
	var error_box = dgeID('error_box');
	var msg_box = dgeID('msg_box');
	msg_box.style.display = "none";
	error_box.style.display = "none";
}

function flip_tags(current_name, new_display){
	var all_links = new Array(
		dgeID('login'),
		dgeID('observe_game'),
		dgeID('new_player'),
		dgeID('new_game')
	);
	var all_divs = new Array(
		dgeID('login_inputs'),
		dgeID('observe_game_inputs'),
		dgeID('new_player_inputs'),
		dgeID('new_game_inputs')
	);

	for(var tag in all_links){
		if(all_links[tag].id != current_name){
			all_links[tag].style.display = new_display;
		}
	}

	for(var tag in all_divs){
		if(all_divs[tag].id != current_name + "_inputs"){
			all_divs[tag].style.display = "none";
		}
	}
}

function click_display(name){
	var link_tag = dgeID(name);
	var div_tag = dgeID(name + "_inputs");

	hide_message_boxes();

	if(div_tag.style.display == "none" || div_tag.style.display == ""){
		flip_tags(name, "none");
		div_tag.style.display = "block";

	} else {
		flip_tags(name, "block");
		div_tag.style.display = "none";
	}
}

function nuke_children(node){
	while(node.children.length > 0){
		node.removeChild(node.children[0]);
	}
}

function player_color_populate(gametype){
	var color_id = dgeID("color_id");
	var colors = gametype["colors"];
	nuke_children(color_id);

	for(var col in colors){
		var color = colors[col];

		var option = dcE("option");
		option.innerText = color[0];
		option.value = col;
		color_id.appendChild(option);
	}
}

function player_template_populate(gametype){
	var template_id = dgeID("template_id");
	var templates = gametype["player_templates"];
	nuke_children(template_id);

	for(var tmp in templates){
		var template = templates[tmp];

		var option = dcE("option");
		option.innerText = template["name"];
		option.value = tmp;
		template_id.appendChild(option);
	}
}

function reset_select_values(){
	var selects = dgeTN("select");

	for(var sel in selects){
		var select = selects[sel];
		select.value = "";
	}
}

function update_game(){
	var game_tag = dgeID("player_game_name");
	var color_id_row = dgeID("color_id_row");
	var template_id_row = dgeID("template_id_row");
	var gametype = window.gametypes[window.games[game_tag.value]];

	if(gametype != undefined && "colors" in gametype){
		color_id_row.style.display = "table-row";
		player_color_populate(gametype);
	} else {
		color_id_row.style.display = "none";
	}

	if(gametype != undefined && gametype["player_templates"].length > 1){
		template_id_row.style.display = "table-row";
		player_template_populate(gametype);
	} else {
		template_id_row.style.display = "none";
	}
}
