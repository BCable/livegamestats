window.players_json = new Array();
window.player_current = {};
window.player_saved = {};
window.timestamp = 0;

function update_player_border(border_color){
	var gameboard = dgeID("gameboard");
	var current_player_board = dgeID("current_player");
	current_player_board.style.border = "2px solid " + border_color;
}

function not_saved(){
	var updated = false;
	for(var attr in window.player_current){
		if(window.player_current[attr] != window.player_saved[attr]){
			updated = true;
			break;
		}
	}
	return updated;
}

function update_border(updated){
	if(updated){
		update_player_border("#C02020");
	} else {
		update_player_border("#A0A0E0");
	}
}

function update_player(attr, val){
	window.player_current[attr] = val;
	var updated = not_saved();
	update_border(updated);

	if(updated){
		var update_button = dgeID("update_button");
		update_button.disabled = false;
	}
}

function send_changes(){
	var changes = new Array();

	for(var item in window.player_current){
		if(window.player_saved[item] != window.player_current[item]){
			changes[item] = window.player_current[item];
		}
	}

	post_json("update", changes, save_callback);
}

function save_changes(phase, send=false, remote=false){
	var updated = not_saved();
	if(!updated){
		return;
	}

	var update_tr = dgeID("update_tr");
	var confirm_tr = dgeID("confirm_tr");

	if(phase == 2){
		if(send){
			send_changes();
		} else if(!remote) {
			save_phase_2();
		}
	}

	if(phase == 1){
		update_tr.style.display = "none";
		confirm_tr.style.display = "table-row";
	} else {
		update_tr.style.display = "table-row";
		confirm_tr.style.display = "none";
	}
}

function save_phase_2(){
	window.player_saved = deepcopy(window.player_current);
	update_border(false);

	var update_button = dgeID("update_button");
	update_button.disabled = true;
}

function save_callback(response){
	if(response == "true"){
		save_phase_2();
	}
}

function get_game_attributes(player_json, attr=null, value=null){
	var game_attributes = window.gametype_json["player_templates"][
		player_json["template_id"]
	]["attributes"];

	if(attr != null && value != null){
		if(game_attributes[attr]["type"] == "select"){
			return game_attributes[attr]["values"][value];
		} else {
			return value;
		}
	}
	return game_attributes;
}

function build_player(player_json, current_player){
	// create board
	var player_board = dcE("div");
	player_board.className = "player_board";
	if(current_player){
		player_board.id = "current_player";
	}

	// mobile fix
	if(screen.availWidth < 1024){
		player_board.style.clear = "both";
	}

	// player name
	var player_name = dcE("span");
	var player_display_name = player_json["name"];
	if(!("colors" in window.gametype_json)){
		player_display_name += " (";
		player_display_name += window.gametype_json["player_templates"][
			player_json["template_id"]
		]["name"];
		player_display_name += ")";
	}
	player_name.className = "player_name";
	player_name.innerHTML = player_display_name;
	player_board.appendChild(player_name);

	// player color indicator
	var color_info;
	if("colors" in window.gametype_json){
		color_info = window.gametype_json["colors"][player_json["color_id"]][1];
	} else {
		color_info = window.gametype_json["player_templates"][
			player_json["template_id"]
		]["color_display"];
	}
	var player_color = dcE("span");
	player_color.className = "player_color";
	player_color.innerHTML = "&nbsp;";
	player_color.style.backgroundColor = color_info;
	player_board.appendChild(player_color);

	// player dynamic table
	var table_attrs = dcE("table");
	table_attrs.className = "player_attrs";
	table_attrs.cellPadding = "0";
	table_attrs.cellSpacing = "0";

	// loop attributes
	var game_attributes = get_game_attributes(player_json);
	var cur_tr = null;
	var maxX = 0;
	var maxX_calc = true;
	for(var attr in game_attributes){
		var game_attr = game_attributes[attr];
		var spanX = 1;
		var spanY = 1;

		if("spanX" in game_attr){
			spanX = game_attr["spanX"];
		}

		if("spanY" in game_attr){
			spanY = game_attr["spanY"];
		}

		if(current_player){
			if(game_attr["new_element"] && attr != 0){
				maxX_calc = false;
			}

			if(maxX_calc){
				maxX += spanX;
			}
		}

		// see if new TR required
		if(cur_tr == null || game_attr["new_element"]){
			if(cur_tr != null){
				table_attrs.appendChild(cur_tr);
			}
			cur_tr = dcE("tr");
			cur_tr.rowSpan = game_attr["spanY"];
		}

		// build TD
		var attr_td = dcE("td");
		if(game_attr["spanX"] != undefined){
			attr_td.colSpan = game_attr["spanX"];
		}

		// text type
		if(game_attr["type"] == "text"){
			var attr_span = document.createElement("span");
			attr_span.innerHTML = game_attr["value"];
			attr_span.style.backgroundColor = game_attr["background-color"];
			attr_span.style.border = game_attr["border"];
			attr_span.style.margin = game_attr["margin"];
			attr_span.style.padding = game_attr["padding"];
			attr_span.style.width = game_attr["width"];
			attr_td.appendChild(attr_span);
		}

		// input type
		else if(game_attr["type"] == "input"){
			if(current_player){
				var input_tag = dcE("input");
				input_tag.autocomplete = "off";
				input_tag.className = "entry";
				input_tag.type = game_attr["input_type"];
				input_tag.name = "attr" + attr;
				input_tag.id =
					"player" + player_json["player_id"] +
					"attr" + attr;
				input_tag.value = game_attr["default"];
				window.player_current["attr" + attr] = game_attr["default"];
				window.player_saved["attr" + attr] = game_attr["default"];

				if("width" in game_attr){
					input_tag.style.width = game_attr["width"];
				}

				input_tag.onchange = function(){
					update_player(this.name, this.value);
				}

				// finalize TD
				attr_td.appendChild(input_tag);

			} else {
				var value_tag = dcE("span");
				value_tag.name = "attr" + attr;
				value_tag.id =
					"player" + player_json["player_id"] +
					"attr" + attr;
				value_tag.innerHTML = game_attr["default"];
				window.player_current["attr" + attr] = game_attr["default"];
				window.player_saved["attr" + attr] = game_attr["default"];

				if("width" in game_attr){
					value_tag.style.width = game_attr["width"];
				}

				// finalize TD
				attr_td.appendChild(value_tag);
			}
		}

		// select type
		else if(game_attr["type"] == "select"){
			if(current_player){
				var select_tag = dcE("select");
				select_tag.autocomplete = "off";
				select_tag.className = "entry";
				select_tag.name = "attr" + attr;
				select_tag.id =
					"player" + player_json["player_id"] +
					"attr" + attr;

				// loop options
				for(var i in game_attr["values"]){
					var value = game_attr["values"][i];

					var option_tag = dcE("option");
					option_tag.innerHTML = value;
					option_tag.value = i;

					if(game_attr["default"] == i){
						window.player_current[
							"attr" + attr
						] = game_attr["default"];
						window.player_saved[
							"attr" + attr
						] = game_attr["default"];
						option_tag.selected = "selected";
					}

					select_tag.appendChild(option_tag);
				}

				select_tag.onchange = function(){
					update_player(this.name, this.value);
				}

				// finalize TD
				attr_td.appendChild(select_tag);

			} else {
				var value_tag = dcE("span");
				value_tag.id =
					"player" + player_json["player_id"] +
					"attr" + attr;
				value_tag.innerHTML = game_attr["values"][
					game_attr["default"]
				];

				if("width" in game_attr){
					value_tag.style.width = game_attr["width"];
				}

				// finalize TD
				attr_td.appendChild(value_tag);
			}
		}

		cur_tr.appendChild(attr_td);
	}

	// final attr TR add
	if(cur_tr != null){
		table_attrs.appendChild(cur_tr);
	}

	// player update button(s)
	if(current_player){
		// phase one
		var update_tr = dcE("tr");
		var update_td = dcE("td");
		var update_button = dcE("input");
		update_tr.id = "update_tr";
		update_button.id = "update_button";
		update_button.disabled = true;
		update_button.type = "button";
		update_button.value = "Update";
		update_button.onclick = function(){ save_changes(1); };
		update_td.colSpan = maxX;
		update_td.appendChild(update_button);
		update_tr.appendChild(update_td);
		table_attrs.appendChild(update_tr);

		// phase two
		var confirm_tr = dcE("tr");
		var confirm_td = dcE("td");
		var confirm_button = dcE("input");
		var cancel_td = dcE("td");
		var cancel_button = dcE("input");
		confirm_tr.id = "confirm_tr";
		cancel_button.id = "cancel_button";
		cancel_button.type = "button";
		cancel_button.value = "Cancel";
		cancel_button.onclick = function(){ save_changes(0); };
		cancel_td.colSpan = Math.floor(maxX/2) + (maxX % 2);
		cancel_td.appendChild(cancel_button);
		confirm_button.id = "confirm_button";
		confirm_button.type = "button";
		confirm_button.value = "Confirm";
		confirm_button.onclick = function(){ save_changes(2, true); };
		confirm_td.colSpan = Math.floor(maxX/2);
		confirm_td.appendChild(confirm_button);
		confirm_tr.appendChild(cancel_td);
		confirm_tr.appendChild(confirm_td);
		table_attrs.appendChild(confirm_tr);
	}

	// add attributes table to player board
	player_board.appendChild(table_attrs);

	// finalize initial state of board and add it to the screen
	gameboard.appendChild(player_board);
}

function adjust_board_widths(){
	var player_boards = dgeCN("player_board");

	// scan for width of all player boards, then update to width of maximum
	if(window.maxWidth == undefined){
		window.maxWidth = 0;
		for(var board in player_boards){
			var player_board = player_boards[board];
			if(
				typeof(player_board) == "object" &&
				player_board.clientWidth > window.maxWidth
			){
				window.maxWidth = player_board.clientWidth;
			}
		}
	}

	for(var board in player_boards){
		var player_board = player_boards[board];
		if(typeof(player_board) == "object"){
			player_board.style.width = window.maxWidth + "px";
		}
	}
}

function build_initial_board(){
	// set title
	var gametitle = dgeID("gametitle");
	var gamecode = dgeID("gamecode");
	var gametitle_text = window.gametype_json["name"];
	gametitle.innerHTML = gametitle_text;
	gamecode.innerHTML = window.game_json["gamecode"];
}

function build_initial_players(){
	// build current player first
	if(window.game_json["player_id"] != null){
		build_player(window.players_json[window.game_json["player_id"]], true);
	}

	// loop remaining players
	for(var player in window.players_json){
		var player_json = window.players_json[player];
		var current_player = false;

		// not current player
		if(player != window.game_json["player_id"]){
			build_player(player_json, false);
		}
	}
}

function main_load_game(players_json){
	players_update_id(players_json);
	build_initial_board();
	if(window.display_gameboard){
		build_initial_players();
	}
	adjust_board_widths();
}

function timestamp_to_string(timestamp){
	var time_string = "";
	var date_obj = new Date(timestamp*1000);

	time_string += date_obj.getFullYear();
	time_string += "-";
	time_string += String("0" + date_obj.getMonth()).slice(-2);
	time_string += "-";
	time_string += String("0" + date_obj.getDate()).slice(-2);
	time_string += " ";
	time_string += String("0" + date_obj.getHours()).slice(-2);
	time_string += ":";
	time_string += String("0" + date_obj.getMinutes()).slice(-2);
	time_string += ":";
	time_string += String("0" + date_obj.getSeconds()).slice(-2);

	return time_string;
}

function player_template_name(player_id, template_id){
	var player = window.players_json[player_id];
	var player_template = window.gametype_json["player_templates"][template_id];

	if("name" in player_template){
		return player_template["name"];
	} else {
		return window.gametype_json["colors"][player["color_id"]][0];
	}
}

function append_event_log(update_json){
	var event_log = dgeID("events");
	var new_span = dcE("span");
	var span_text = timestamp_to_string(update_json["timestamp"]);

	var player = window.players_json[update_json["player_id"]];
	var player_template = window.gametype_json["player_templates"][
		player["template_id"]
	];

	span_text += " - ";

	if("template_id" in update_json){
		new_span.className = "event_player";
		span_text += "Player \"" + update_json["name"] + "\" Joined as \"";
		span_text += player_template_name(
			update_json["player_id"], player["template_id"]
		);
		span_text += "\"";

	} else {
		var attribute = player_template["attributes"][
			update_json["attribute_id"]
		];

		new_span.className = "event_value";
		span_text += "(";
		span_text += player["name"];
		span_text += ") - ";
		span_text += attribute["name"];
		span_text += " updated to ";

		if(attribute["type"] == "select"){
			span_text += attribute["values"][update_json["new_value"]];
		} else {
			span_text += update_json["new_value"];
		}
	}

	new_span.innerHTML = span_text;
	if(event_log.childNodes.length == 0){
		event_log.appendChild(new_span);
	} else {
		event_log.insertBefore(new_span, event_log.childNodes[0]);
	}
}

function fast_forward_game(events_json, players_json, initial=false){
	var updates_json = new Array();

	if(events_json.length > 0){
		updates_json = updates_json.concat(events_json);
	}

	if(players_json.length > 0){
		updates_json = updates_json.concat(players_json);
	}

	if(updates_json.length > 0){
		updates_json.sort(function(a,b){
			return a["timestamp"]-b["timestamp"];
		});

		for(var up in updates_json){
			var update_json = updates_json[up];

			if(window.display_gameboard){
				if("template_id" in update_json){
					if(!initial){
						var player_json = update_json;
						build_player(player_json, false);
					}

				} else {
					var event_json = update_json;
					var player_json = window.players_json[
						event_json["player_id"]
					];
					var update_tag = dgeID(
						"player" + event_json["player_id"] +
						"attr" + event_json["attribute_id"]
					);

					if(update_tag == null){
						continue;
					}

					if(update_tag.tagName.toLowerCase() == "span"){
						update_tag.innerHTML = get_game_attributes(
							player_json, event_json["attribute_id"],
							event_json["new_value"]
						);

						if(!initial){
							update_tag.className = "updated";
							setTimeout(function(){
								var updated_tags = dgeCN("updated");
								for(var ut in updated_tags){
									updated_tags[ut].className = "not_updated";
								}
							}, 5000);
						}

					} else {
						update_tag.value = event_json["new_value"];
						update_player(
							"attr" + event_json["attribute_id"],
							event_json["new_value"]
						);
					}
				}
			}

			if(window.display_events){
				append_event_log(update_json);
			}
		}
		save_changes(2, false, !initial);
	}
}

function players_update_id(){
	var player_id = 0;

	for(var player in window.players_json){
		window.players_json[player]["player_id"] = player_id;
		player_id++;
	}
}

function update_backend(callback){
	load_json("backend/" + window.timestamp, callback);
}


function update_timestamp(events_json, players_json){
	var new_players_timestamp = 0;
	if(players_json.length > 0){
		new_players_timestamp = parseInt(
			players_json[players_json.length-1]["timestamp"]
		);
	}

	var new_events_timestamp = 0;
	if(events_json.length > 0){
		new_events_timestamp = parseInt(
			events_json[events_json.length-1]["timestamp"]
		);
	}

	if(new_players_timestamp > window.timestamp){
		window.timestamp = new_players_timestamp;
	}
	if(new_events_timestamp > window.timestamp){
		window.timestamp = new_events_timestamp;
	}
}

function load_game(game_id, gametype, gamecode, player_id){
	window.game_json = {
		"game_id": game_id,
		"gametype": gametype,
		"gamecode": gamecode,
		"player_id": player_id
	};

	load_json("../gameconfig/" + gametype + ".json", function(response){
		window.gametype_json = JSON.parse(response);

		update_backend(function(response){
			var backend_json = JSON.parse(response);
			var events_json = backend_json["event"];
			var players_json = backend_json["player"];

			window.players_json = players_json;
			main_load_game(players_json);

			fast_forward_game(events_json, players_json, true);
			update_timestamp(events_json, players_json);
		});

		setInterval(function(){
			update_backend(function(response){
				var backend_json = JSON.parse(response);
				var events_json = backend_json["event"];
				var players_json = backend_json["player"];

				window.players_json = window.players_json.concat(players_json);
				players_update_id(players_json);

				fast_forward_game(events_json, players_json);
				adjust_board_widths();
				update_timestamp(events_json, players_json);
			});
		}, 3000);
	});
}
