function load_json(filename, callback){
	var xhr = new XMLHttpRequest();
	xhr.overrideMimeType("application/json");

	xhr.open("GET", filename, true);
	xhr.onreadystatechange = function(){
		if(xhr.readyState == 4 && xhr.status == "200"){
			callback(xhr.responseText);
		}
	};
	xhr.send(null);  
}

function array_to_post(array){
	var post_string = "";

	for(var val in array){
		if(post_string != ""){
			post_string += "&";
		}
		post_string += val + "=" + encodeURIComponent(array[val]);
	}

	return post_string;
}

function post_json(filename, post_array, callback){
	var xhr = new XMLHttpRequest();

	xhr.open("POST", filename, true);
	xhr.setRequestHeader("Content-type", "application/x-www-form-urlencoded");
	xhr.onreadystatechange = function(){
		if(xhr.readyState == 4 && xhr.status == "200"){
			callback(xhr.responseText);
		}
	};
	xhr.send(array_to_post(post_array));
}

function dgeID(id){
	return document.getElementById(id);
}

function dgeN(name){
	return document.getElementsByName(name);
}

function dgeCN(className){
	return document.getElementsByClassName(className);
}

function dgeTN(name){
	return document.getElementsByTagName(name);
}

function dcE(elem){
	return document.createElement(elem);
}

function dcTN(text){
	return document.createTextNode(text);
}

function deepcopy(old_obj){
	var new_obj = {};

	for(var i in old_obj){
		if(typeof(old_obj[i]) == "object"){
			new_obj[i] = deepcopy(old_obj[i]);
		} else {
			new_obj[i] = old_obj[i];
		}
	}

	return new_obj;
}
