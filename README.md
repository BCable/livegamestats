livegamestats 1.0.2
===================

Requirements: Apache HTTPD, MariaDB (MySQL, or other PDO compatible database), PHP, PHP-PDO

## Currently Supported Games

 * Betrayal at the House on the Hill
 * Eclipse

## Tested Browsers

Android: Chrome, Firefox

iOS: Firefox, Safari

Linux: Chrome, Firefox

Windows: Chrome, Firefox, Edge

## Screenshots

Android & Firefox:

![alt text](https://bcable.github.io/livegamestats/screenshot_01.png "Android & Firefox")

iOS & Firefox:

![alt text](https://bcable.github.io/livegamestats/screenshot_02.png "iOS & Firefox")
